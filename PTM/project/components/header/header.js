import { getUser } from "../../js/providers/users.js";

export const init = () => {
    console.log('Iniciando header...');
    //get user
    getUser().then( (response) => {
        if(response.status == 0)
            showUser(response.user);
    });
};

//show user infor 
function showUser(user) {
    console.log(user);
    //image
    document.getElementById('img-user-photo').src =user.photo;
    //user infor
    document.getElementById('label-user-name').textContent = user.name;
    document.getElementById('label-user-role').textContent = user.role.admin;

}