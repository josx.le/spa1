import { menu } from "./settings.js";

let currentLanguage = 0; // 0 para español, 1 para inglés

export const init = () => {
    console.log('Initializing sidemenu...');
    drawMenu();
    CambiarLenguaje();
    // Initialize default content
    loadContent('home'); // Default to 'home'
}

function drawMenu() {
    const parent = document.getElementById('sidemenu');
    menu.forEach(option => {
        drawMenuOption(option);
    });
}

function drawMenuOption(option) {
    console.log(option);

    // parent div
    var parent = document.getElementById('sidemenu');

    // create options div
    var divOptions = document.createElement('div');
    divOptions.id = 'sidemenu-option-' + option.module;
    divOptions.className = 'sidemenu-option';
    parent.appendChild(divOptions);

    // icon
    var divIcon = document.createElement('div');
    divIcon.className = 'sidemenu-icon';
    divIcon.style.backgroundColor = 'var(--' + option.module + ')';
    divOptions.appendChild(divIcon);
    
    var icon = document.createElement('i');
    icon.className = 'fas fa-' + option.icon;
    divIcon.appendChild(icon);

    // title
    var divTitle = document.createElement('div');
    divTitle.className = 'sidemenu-title';
    divTitle.textContent = option.title[currentLanguage];
    divOptions.appendChild(divTitle);

    // Event listener for menu option click
    divOptions.addEventListener('click', function() {
        loadContent(option.module);
    });

    // Check for submenu
    if (typeof option.submenu !== 'undefined') {
        var chevronButton = document.createElement('button');
        chevronButton.className = 'submenu-toggle'; 
        chevronButton.innerHTML = '<i class="fas fa-chevron-down"></i>'; 
        chevronButton.style.marginLeft = 'auto'; 
        divOptions.appendChild(chevronButton);

        drawSubMenu(option.submenu, divOptions);

        chevronButton.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent triggering parent click event
            toggleSubMenu(divOptions);
        });
    }
}

function CambiarLenguaje() {
    const languageIcon = document.querySelector('.fa-language');
    languageIcon.addEventListener('click', () => {
        currentLanguage = (currentLanguage + 1) % 2; // Alterna entre 0 y 1
        updateMenuTitles();
    });
}

function updateMenuTitles() {
    menu.forEach(option => {
        const divTitle = document.querySelector(`#sidemenu-option-${option.module} .sidemenu-title`);
        if (divTitle) {
            divTitle.textContent = option.title[currentLanguage];
        }
        if (option.submenu) {
            option.submenu.forEach(subOption => {
                const subDivTitle = document.querySelector(`#submenu-option-${subOption.module} .submenu-title`);
                if (subDivTitle) {
                    subDivTitle.textContent = subOption.title[currentLanguage];
                }
            });
        }
    });
}

function drawSubMenu(submenu, parent) {
    var submenuContainer = document.createElement('div');
    submenuContainer.className = 'submenu-container';

    submenu.forEach(subOption => {
        var subItem = document.createElement('div');
        subItem.className = 'submenu-item';
        subItem.id = 'submenu-option-' + subOption.module;

        var subIcon = document.createElement('i');
        subIcon.className = 'fas fa-' + subOption.icon;
        subItem.appendChild(subIcon);

        var subLabel = document.createElement('span');
        subLabel.className = 'submenu-title';
        subLabel.textContent = subOption.title[currentLanguage];
        subItem.appendChild(subLabel);

        subItem.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent triggering parent click event
            loadContent(subOption.module);
        });

        submenuContainer.appendChild(subItem);
    });

    parent.appendChild(submenuContainer);
}

function toggleSubMenu(optionElement) {
    var submenuContainer = optionElement.querySelector('.submenu-container');
    submenuContainer.classList.toggle('show'); 
}

function loadContent(module) {
    const content = document.getElementById('content');
    content.innerHTML = ''; // limpiador

    const modulePath = `components/${module}/${module}`;

    // Load HTML content
    fetch(`${modulePath}.html`)
        .then(response => response.text())
        .then(html => {
            content.innerHTML = html;

            // para que cargue el  CSS
            const oldStylesheet = document.getElementById('dynamic-stylesheet');
            if (oldStylesheet) {
                oldStylesheet.remove();
            }
            const newStylesheet = document.createElement('link');
            newStylesheet.rel = 'stylesheet';
            newStylesheet.href = `${modulePath}.css`;
            newStylesheet.id = 'dynamic-stylesheet';
            document.head.appendChild(newStylesheet);

            // para que cargue el JS
            const oldScript = document.getElementById('dynamic-script');
            if (oldScript) {
                oldScript.remove();
            }
            const newScript = document.createElement('script');
            newScript.src = `${modulePath}.js`;
            newScript.id = 'dynamic-script';
            document.body.appendChild(newScript);
        })
        .catch(error => {
            console.error('Error loading content:', error);
            content.innerHTML = currentLanguage === 0 ? 'Error al cargar el contenido' : 'Error loading content';
        });
}

// Llama a la función init cuando el DOM esté completamente cargado
document.addEventListener('DOMContentLoaded', init);

// document.getElementById('sidemenu').style.display = 'none'; // para quitalo

// document.getElementById('sidemenu').style.display = 'block'; // para tenerlo

const barsIcon = document.querySelector('.fas.fa-bars');
const sidemenu = document.getElementById('sidemenu');

barsIcon.addEventListener('click', function() {
    if (sidemenu.style.display === 'none') {
        sidemenu.style.display = 'block';
    } else {
        sidemenu.style.display = 'none';
    }
});

