export var menu = [
    {
        module: 'home',
        icon: 'home',
        title: ['inicio', 'home'],
        components: 'components/home'
    },
    {
        module: 'dashboard',
        icon: 'chart-bar',
        title: ['Tablero', 'Dashboard'],
        components: 'components/dashboard'
    },
    {
        module: 'layout',
        icon: 'map',
        title: ['Plano', 'Layout'],
        components: 'components/layout'
    },
    {
        module: 'settings',
        icon: 'cog',
        title: ['Configuración', 'Settings'],
        components: 'components/settings',
        submenu: [
            {
                module: 'sensors',
                icon: 'tachometer-alt',
                title: ['Sensores', 'Sensors'],
                components: 'components/sensors'
            },
            {
                module: 'areas',
                icon: 'sliders-h',
                title: ['Áreas', 'Areas'],
                components: 'components/areas' 
            }
        ]
    },
    {
        module: 'logs',
        icon: 'th-list',
        title: ['Bitacora', 'Log'],
        components: 'components/logs'
    }
]