export async function loadComponent(options){
    // split url
    var urlParts = options.url.split('/');
    var fileName = urlParts[urlParts.length - 1];
    // request and module urls
    var now = new Date();
    var requestUrl = window.location.href + options.url + '/' + fileName +  '.html' + '?a=' + now.getTime();
    var moduleUrl = '../../' + options.url + '/' + fileName + '.js';
    // get components
    console.log('Loading Component ' + requestUrl);
    return await fetch(requestUrl, {header: {'pragma': 'no-cache', 'Cache-control': 'no-cache', 'cache': 'no-store'}})
    .then((response) => response.text())
    .then((html) => { document.getElementById(options.parent).innerHTML = html; })
    .then(() => { importModule(moduleUrl) });
}

// import module
async function importModule(moduleUrl){
    console.log('Importing Module ' + moduleUrl);
    let { init } = await import(moduleUrl);
    init();
}
