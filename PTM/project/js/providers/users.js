import { settings } from "./setting.js";

export async function getUser(){
    var url = settings.apiUrl + "data/user.json";
    console.log(url)
    return fetch(url, {
        headers: {
            'pragma': 'no-cahe',
            'Cache-Control': 'no-cache',
            'cache': 'no-store'
        }
    })
    .then((Response) => Response.text())
    .then((data) => { return JSON.parse(data);})
}