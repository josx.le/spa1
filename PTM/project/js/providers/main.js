//imports 
import { settings } from './setting.js';
import { loadComponent } from './components.js';

//event handler
window.addEventListener('load', load);

//load document 
function load(){
    console.log('Loading Main...');
    settings.load.components.forEach(c => {
        console.log(c);
        /* loadComponent({
            parent: c.parent,
            url: c.url
        }); */
        loadComponent(c);
    });
}